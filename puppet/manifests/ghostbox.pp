class { 'apt': }

Exec['apt-get-update'] -> Package <| |>

exec {'apt-get-update':
    command => "apt-get update",
    path => ['/bin', '/usr/bin', '/sbin']
}

class { 'ghost':
    environment => 'development',
    development_url  => 'http://127.0.0.1',
    development_host => '127.0.0.1',
    development_port => 2368
}
