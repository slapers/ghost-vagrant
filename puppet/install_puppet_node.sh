#!/bin/bash
#


# RUNTIME VARIABLES, should not be change in normal circumstances
#-----------------------------------------------------------------------------
TOUCH='/usr/bin/touch'
WGET='/usr/bin/wget'
APTGET='/usr/bin/apt-get'
DPKG='/usr/bin/dpkg'
RM='/bin/rm'
SERVICE='/usr/sbin/service'
CAT='/bin/cat'
HOSTNAME='/bin/hostname'
PUPPET='/usr/bin/puppet'
ECHO='/bin/echo'




#-----------------------------------------------------------------------------
# Make sure we dont run a second time
#-----------------------------------------------------------------------------

if [ -f /root/install_puppet_ok ]
then
  echo 'Not installing puppet, install script already ran'
  exit
fi
$TOUCH /root/install_puppet_ok





#-----------------------------------------------------------------------------
# Install the puppet packages
#-----------------------------------------------------------------------------

$APTGET -y update
$APTGET -y install wget
$WGET http://apt.puppetlabs.com/puppetlabs-release-precise.deb
$DPKG -i puppetlabs-release-precise.deb
$RM -f puppetlabs-release-precise.deb

$APTGET -y update
$APTGET -y install puppet


exit 0



